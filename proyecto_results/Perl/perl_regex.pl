#!/usr/bin/perl

use strict;
use warnings;

my $t 		= time;
my $match 	= 0;
my $nomatch = 0;

open(my $f, "<results.csv") or die ("no hay archivo");

while(<$f>) {
	# quitar caracteres raros.
	chomp;

	# comenzamos con las regex.
	# 1875-03-06,England,Scotland,2,2,Friendly,London,England,FALSE
	if (m/^([\d]{4,4})\-.*?,(.*?),(.*?),(\d+),(\d+),.*$/) {
		# mostramos cuándo el país visitante le ganó al local.
		if ($5 > $4) {
			printf("%d: %s (%d) - (%d) %s\n",
				$1, $2, $4, $5, $3
			);
		}
		$match++;
	} else {
		$nomatch++;
	}
}
close ($f);

printf("Se encontraron: \n - %d matches\n - %d no matches\ntardo %d segundos\n", $match, $nomatch, time() - $t);